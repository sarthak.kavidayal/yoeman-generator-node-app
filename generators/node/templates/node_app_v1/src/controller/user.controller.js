const log =  require("../logger")

const controller = (req, res) => {
    try {
        res.status(200).send('ok')
    }
    catch(e) {
        log.error(e)
        res.status(500).send('error')
    }
}

module.exports =  controller