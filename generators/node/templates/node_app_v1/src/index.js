const express = require('express')
const log = require("./logger") 
const userRouter = require('./routers/index')

const app = express()
const PORT = process.env.PORT || 8080

app.use(express.json({limit: "1mb"}))
app.use(userRouter)

app.listen(PORT, () => {
    log.info(`listining at port ${PORT}`)
})