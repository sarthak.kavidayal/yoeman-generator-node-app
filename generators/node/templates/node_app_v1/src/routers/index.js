const express = require('express')
const middleware = require('../middelware/index')
const controller = require('../controller/user.controller')

const router = new express.Router()

router.get('/getData' ,middleware, controller)

router.post('/setData', middleware, controller)

module.exports = router