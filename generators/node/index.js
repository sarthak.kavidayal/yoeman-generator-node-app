const Generator = require('yeoman-generator')

module.exports = class extends Generator {
    initializing() {
        this.log('Custom-node-app generator:')
    }

    async prompting() {
        this.answers = await this.prompt([
            {
                type: 'input',
                name: 'name',
                message: 'Your project name',
                default: this.appname.replace(/ /g, '-')
            },
            {
                type: 'input',
                name: 'description',
                message: 'Please enter a description for your project',
                default: ''
            }
        ])
    }

    writing() {
        this.log('writing...')

        this.fs.copyTpl(this.templatePath('node_app_v1'), this.destinationPath('./'), {
            name: this.answers.name,
            description: this.answers.description
        })

        this.fs.copyTpl(
            this.templatePath('_README.md'),
            this.destinationPath('README.md'),
            {
              name: this.answers.name,
              description: this.answers.description
            }
        )

        this.fs.copyTpl(
            this.templatePath('_gitignore'),
            this.destinationPath('.gitignore'),
            {
              name: this.answers.name,
              description: this.answers.description
            }
        )
    }

    end() {
        this.log(`
-----------------------------------------------------------------------------------------
-> To run the project locally write "npm run dev" in terminal at the root of the project"
-----------------------------------------------------------------------------------------
        `)
    }
}