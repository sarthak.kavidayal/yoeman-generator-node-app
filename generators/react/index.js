const Generator = require("yeoman-generator");

module.exports = class extends Generator {
  initializing() {
    this.log("Custom react generator:");
  }

  writing() {
    this.log("writing...");

    this.fs.copyTpl(
      this.templatePath("yeomanreact"),
      this.destinationPath("./")
    );
  }

  end() {
    this.log(`
-----------------------------------------------------------------------------------------
    Completed!
-----------------------------------------------------------------------------------------
        `);
  }
};
