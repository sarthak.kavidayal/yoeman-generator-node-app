import { combineReducers } from "redux";
import Loader from "redux/reducers/loader";

export default combineReducers({
  Loader,
});
