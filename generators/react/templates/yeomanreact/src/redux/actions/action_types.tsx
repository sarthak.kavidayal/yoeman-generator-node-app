export const SHOW_APP_LOADER = "SHOW_APP_LOADER";
export const HIDE_APP_LOADER = "HIDE_APP_LOADER";
export const USER_INITIATE = "USER_INITIATE";
export const SIGN_IN = "SIGN_IN";
