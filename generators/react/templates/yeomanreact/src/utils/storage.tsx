export const storage = {
  set(key: any, value: any) {
    if (
      typeof value === "object" ||
      Array.isArray(value) ||
      Number.isInteger(value) ||
      isNaN(value)
    ) {
      value = JSON.stringify(value);
    }

    localStorage.setItem(key, value);
  },
  get(key: any, defaultValue: any) {
    const data: any = localStorage.getItem(key);

    try {
      let parsed = JSON.parse(data);

      return parsed !== null ? parsed : defaultValue;
    } catch (e) {
      return data !== null ? data : defaultValue;
    }
  },
  remove(key: any) {
    return localStorage.removeItem(key);
  },
  clear() {
    return localStorage.clear();
  },
};
