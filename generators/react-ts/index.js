const Generator = require("yeoman-generator");

module.exports = class extends Generator {
  initializing() {
    this.log("Custom node-ts generator:");
  }

  writing() {
    this.log("writing...");

    this.fs.copyTpl(this.templatePath("react-ts"), this.destinationPath("./"));
    this.fs.copyTpl(
      this.templatePath("_eslintignore"),
      this.destinationPath(".eslintignore")
    );
    this.fs.copyTpl(
      this.templatePath("_eslintrc.js"),
      this.destinationPath(".eslintrc.js")
    );
    this.fs.copyTpl(
      this.templatePath("_gitignore"),
      this.destinationPath(".gitignore")
    );
    this.fs.copyTpl(
      this.templatePath("_prettierrc.js"),
      this.destinationPath(".prettierrc.js")
    );
  }

  end() {
    this.log(`
-----------------------------------------------------------------------------------------
  Completed!
-----------------------------------------------------------------------------------------
        `);
  }
};
