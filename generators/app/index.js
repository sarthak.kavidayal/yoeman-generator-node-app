const Generator = require("yeoman-generator");

module.exports = class extends Generator {
  initializing() {
    this.log(`
        Mention subgenerator --

        eg:
        
        yo wits-test:node
        yo wits-test:react
        yo wits-test:nextjs
        yo wits-test:react-ts
        yo wits-test:node-ts`);
  }
};
