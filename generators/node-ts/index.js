const Generator = require("yeoman-generator");

module.exports = class extends Generator {
  initializing() {
    this.log("Custom node-ts generator:");
  }

  writing() {
    this.log("writing...");

    this.fs.copyTpl(this.templatePath("node-ts"), this.destinationPath("./"));
    this.fs.copyTpl(
      this.templatePath("_eslintignore"),
      this.destinationPath(".eslintignore")
    );
    this.fs.copyTpl(
      this.templatePath("_eslintrc.js"),
      this.destinationPath(".eslintrc.js")
    );
    this.fs.copyTpl(
      this.templatePath("_gitignore"),
      this.destinationPath(".gitignore")
    );
    this.fs.copyTpl(
      this.templatePath("_prettierrc.js"),
      this.destinationPath(".prettierrc.js")
    );
    this.fs.copyTpl(this.templatePath("_env"), this.destinationPath(".env"));
    this.fs.copyTpl(
      this.templatePath("_env.example"),
      this.destinationPath(".env.example")
    );
  }

  end() {
    this.log(`
-----------------------------------------------------------------------------------------
  Completed!
-----------------------------------------------------------------------------------------
        `);
  }
};
