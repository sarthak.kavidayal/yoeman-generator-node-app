import axioPath from "api/axioPath";
import { SIGN_IN } from "redux/actions/action_types";
import ApiRoutes from "api/apiRoutes";

export const auth = (data: any) => (dispatch: any) => {
  return axioPath
    .post(ApiRoutes.login, data)
    .then((response) => {
      dispatch({ type: SIGN_IN, payload: response.data });
      return { error: false, data: response.data };
    })
    .catch((ex) => {
      return { error: true, data: ex };
    });
};
