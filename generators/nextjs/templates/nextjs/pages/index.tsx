import React from "react";
/**
 * Home Page of the Application
 * @return {JSX.Element}
 */
export default function Home(): JSX.Element {
  return (
    <div>
      <h1>Welcome to Next Js BoilerPlace with typeScript</h1>
    </div>
  );
}
