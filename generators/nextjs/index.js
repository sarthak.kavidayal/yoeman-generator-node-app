const Generator = require("yeoman-generator");

module.exports = class extends Generator {
  initializing() {
    this.log("Custom next-js generator:");
  }

  writing() {
    this.log("writing...");

    this.fs.copyTpl(this.templatePath("nextjs"), this.destinationPath("./"));
    this.fs.copyTpl(
      this.templatePath("_husky"),
      this.destinationPath(".husky")
    );
    // this.fs.copyTpl(this.templatePath('_next'), this.destinationPath('./'))
    this.fs.copyTpl(
      this.templatePath("_vscode"),
      this.destinationPath(".vscode")
    );
    this.fs.copyTpl(this.templatePath("_temp"), this.destinationPath(".git"));
    this.fs.copyTpl(
      this.templatePath("_eslintignore"),
      this.destinationPath(".eslintignore")
    );
    this.fs.copyTpl(
      this.templatePath("_eslintrc.js"),
      this.destinationPath(".eslintrc.js")
    );
    this.fs.copyTpl(
      this.templatePath("_gitignore"),
      this.destinationPath(".gitignore")
    );
    this.fs.copyTpl(
      this.templatePath("_prettierignore"),
      this.destinationPath(".prettierignoew")
    );
    this.fs.copyTpl(
      this.templatePath("_prettierrc"),
      this.destinationPath(".prettierrc")
    );
  }

  end() {
    this.log(`
-----------------------------------------------------------------------------------------
Completed!
-----------------------------------------------------------------------------------------
        `);
  }
};
