## Yoeman Generator For Node.js and React.js

Install the package globally.

```
npm i -g generator-wits-test
```

# Requirement

Yoeman genrator should be globally installed.

```
npm i -g yo
```

# Working

To check all subgenerators

```
yo wits-test
```

To use React template

```
yo wits-test:react
```

in the directory where you want to generate the react template

To use Node-typescript template

```
yo wits-test:node-ts
```

in the directory where you want to generate the node template
